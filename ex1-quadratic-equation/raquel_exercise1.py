import math as mt


#Solution 
print('Given:')
print("v\u2080 = 10 m/s") 
print("a = 2.00 m/s\u00b2")
print("x = 200 m")

a = 1.00
b = 10
c = -200

# calculate the discriminant
d = (b**2) - (4*a*c)

# find two solutions
sol1 = (-b - mt.sqrt(d))/(2*a)
sol2 = (-b + mt.sqrt(d))/(2*a)

print('answer')
print("t = {0} and {1}".format(sol2,sol1))

#Since second solution gives us a negative value, we will disregard it. 

#Thus

print("t = 10.0 s")