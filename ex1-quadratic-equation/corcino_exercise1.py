print("Suppose a car ,merges into freeway traffic on a 200-m-long ramp. If its initial velocity is 10 m/s amd it accelerates at 2 m/s^2, how long does it take the car to travel the 200 m up the ramp?")
print("di=200")
print("df=0")
print("a=2")
print("v=10")
print("3rd Kinemtic Formula d=vt+(0.5)at^2")
print("Quadratic Form, 0=(0.5)at^2+vt-d")
print("Thus, by using the quadratic formula, we get")
import math as mt 
a=0.5*2
b=10
c=-200
t1=(-b+mt.sqrt(b**2-4*a*c))/(2*a)
t2=(-b-mt.sqrt(b**2-4*a*c))/(2*a)
print(t1)
print(t2)
print("in which the positive value is the time (in seconds) it takes the car to travel the ramp")