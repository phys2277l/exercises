import math as math
#Given kinematic equation: x=1/2At^2+v_0t+x_0
#For time: 1/2At^2+v_0t+x=0

a=1
v_0=10
x=-200

t_1=(-v_0+math.sqrt(v_0**2-4*a*x))/2*a
t_2=(-v_0-math.sqrt(v_0**2-4*a*x))/2*a

print(t_1)
print(t_2)

#Only time t_1 is valid as there is no negative time