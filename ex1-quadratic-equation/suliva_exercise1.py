#John Martin Suliva



#Assignment in Computational Physics: Calculating Time


#Given

# acceleration = 2 m/s^2
# initial velocity = 10 m/s
# final position = 200 m
# initial position = 0 m

#Unknown

# t = ?

#Equation

# (at**2/2 + v sub 0*t - x) = 0
# t**2 + 10*t - 200 = 0

import math as mt

a = 1
b = 10
c = - 200

print ((-b + mt.sqrt(b**2 - 4*a*c))/2*a)

# ((-b + mt.sqrt(b**2 - 4*a*c))/2*a) is not applicable because of the improbability of negative time