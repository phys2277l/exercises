[1,2,3,22,44,66]
[4,5,6,88,99,11]
[7,8,9,77,55,33]
[10,11,12,34,56,78]
[13,14,15,76,87,98]
[16,17,18,47,36,25]

mylist1 = [[1,2,3,22,44,66],[4,5,6,88,99,11],[7,8,9,77,55,33],[10,11,12,34,56,78],[13,14,15,76,87,98],[16,17,18,47,36,25]]

#All Diagonal values
for i in range(6):
    print(mylist1[i][i])

#All First Row
for j in range(1):
        print(mylist1[0])

#All Last Row
for j in range(1):
        print(mylist1[5])

#Third Column
for j in range(6):
        print(mylist1[j][2])


#Sum of All Diagonal Values
x=0
for i in range(6):
    x += mylist1[i][i]

print(x)

#Average of the 5th row
y=0
for j in range(6):
    y += (mylist1[4][j])

avg5thR=y/6
print(avg5thR)


#Average of the 3rd column
y=0
for j in range(6):
    y += (mylist1[j][2])

avg3rdC = y/6
print(avg3rdC)


#All the values in the list (Use nested for-loop)        
n = 6
for i in range(n):
    for j in range(n):
        print("i:", i, "j:", j)
        print(mylist1[i][j])

