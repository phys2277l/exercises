mynestedlist = [[1, 2, 3, 4, 5, 6],[7, 8, 9, 10, 11, 12],[13, 14, 15, 16, 17, 18],[19, 20, 21, 22, 23, 24],[25, 26, 27, 28, 29, 30],[31, 32, 33, 34, 35, 36]]

#All diagonal values
n = 6
for i in range(n):
    print(mynestedlist[i][i])

#First row
n = 1
for i in range(n):
    print(mynestedlist[0])

#Last row
n = 1
for i in range(n):
    print(mynestedlist[5])

#Third Column
n = 6
for i in range(n):
    print(mynestedlist[i][2])

#Sum of all the diagonal values
sum = 0
n = 6
for i in range(n):
    sum += mynestedlist[i][i]

print(sum)

#Average of the 5th row
average = 0
n = 6
for i in range(n):
    average += mynestedlist[4][i]

print(average/6)

#Average of 3rd column
average3 = 0
n = 6
for i in range(n):
    average3 += mynestedlist[i][2]


print(average3/6)


#Print all loop inside another loop
n = 6
for i in range(n):
    for j in range(n):
        print("i: ", i, "j: ", j)
        print(mynestedlist[i][j])