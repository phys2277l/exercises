print ("Number 1")
import math as mt
import matplotlib.pyplot as plt


def tra (posx, posy, ang, g = 9.8, velo = 10):
    a = []
    b = []

    t = 0
    
    vx0 = velo * mt.cos (ang)
    vy0 = velo * mt.sin (ang)
    
    while posy > 0:
         x = posx + vx0 * t
         y = posy + vy0 * t - g*t**2 / 2
         
         t += 0.03
         
         a.append (x)
         b.append (y)

         print (x, y)
         if y <= 0:
             break
    

    return a, b

posx = 0
posy = 1

#Radians to Degrees
g = (30*mt.pi) / 180
h = (45*mt.pi) / 180

ang1 = g
ang2 = h

a, b = tra (posx, posy, ang1, g = 9.8, velo = 10)
c, d = tra (posx, posy, ang2, g = 9.8, velo = 10)
plt.plot (a,b, '--')
plt.plot (c,d, '-')
plt.show ()