import math
import matplotlib.pyplot as plt

# Constants
g = 9.81  # gravitational acceleration

# Initial conditions
v0 = 30.0  # initial velocity
y0 = 0.0  # initial height
t0 = 0.0  # initial time
theta1 = 30.0  # angle of launch in degrees
theta2 = 60.0

# Convert angles to radians
theta1 = math.radians(theta1)
theta2 = math.radians(theta2)

# Calculate initial velocity components
vx0 = v0 * math.cos(theta1)
vy0 = v0 * math.sin(theta1)

# Initialize arrays for position and time
x1 = [0.0]
y1 = [y0]
t1 = [t0]

x2 = [0.0]
y2 = [y0]
t2 = [t0]

# Launch object
while y1[-1] >= 0.0:
    # Calculate new position and time
    t1_new = t1[-1] + 0.01
    x1_new = x1[-1] + vx0 * 0.01
    y1_new = y1[-1] + vy0 * 0.01 - 0.5 * g * (0.01 ** 2)
    
    # Append new values to arrays
    t1.append(t1_new)
    x1.append(x1_new)
    y1.append(y1_new)
    
    # Calculate new velocity components
    vx1 = vx0
    vy1 = vy0 - g * 0.01
    
    # Update initial conditions for next iteration
    vx0 = vx1
    vy0 = vy1
    
while y2[-1] >= 0.0:
    # Calculate new position and time
    t2_new = t2[-1] + 0.01
    x2_new = x2[-1] + v0 * math.cos(theta2) * 0.01
    y2_new = y2[-1] + v0 * math.sin(theta2) * 0.01 - 0.5 * g * (0.01 ** 2)
    
    # Append new values to arrays
    t2.append(t2_new)
    x2.append(x2_new)
    y2.append(y2_new)

    # Calculate new velocity components
    vx2 = v0 * math.cos(theta2)
    vy2 = v0 * math.sin(theta2) - g * 0.01
    
    # Update initial conditions for next iteration
    v0 = math.sqrt(vx2 ** 2 + vy2 ** 2)
    theta2 = math.atan2(vy2, vx2)

# Plot the results
plt.plot(x1, y1, 'b-', label='Launch Angle = 30 degrees')
plt.plot(x2, y2, 'g-', label='Launch Angle = 60 degrees')
plt.legend(loc='upper right')
plt.xlabel('Horizontal distance (m)')
plt.ylabel('Height (m)')
plt.show()
