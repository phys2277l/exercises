import matplotlib.pyplot as plt
import math as math

def traj(x_0,y_0,v,angle,g=9.8):
    x=[]
    y=[]
    vx=v*math.cos(angle)
    vy=v*math.sin(angle)
    t=0
    while y_0>0:
        t+=0.2
        x1=x_0 + vx*t
        y1=y_0 + vy*t - (g/2)*t**2
        x.append(x1)
        y.append(y1)
        if y1<=0:
            break  
    print(x,y)
    return x,y


v=100
angle1=0.6
angle2=0.785398
x_0=1
y_0=1
x,y=traj(x_0,y_0,v,angle1)
x2,y2=traj(x_0,y_0,v,angle2)
plt.plot(x,y)
plt.plot(x2,y2)

#This was run in Jupyter