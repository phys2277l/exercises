#1
import matplotlib.pyplot as plt 
import math as mt
def trajectory(angle,v,x_0,y_0,g=9.8):
    x=[]
    y=[]
    V_0x=v*mt.cos(angle)
    V_0y=v*mt.sin(angle)
    t=0
    while y_0>=0:
        t+=0.2
        x1= x_0 + V_0x*t
        y1= y_0 + V_0y*t -1/2*g*t**2
        x.append(x1)
        y.append(y1)
        if y1<1:
            break
    return x,y
angle=45
v=50 
y_0=0
x_0=0
x,y=trajectory(angle,v,x_0,y_0,g=9.8)
angle1=20
x1,y1=trajectory(angle1,v,x_0,y_0,g=9.8)

plt.plot(x1,y1)
plt.plot(x,y)
plt.show()

