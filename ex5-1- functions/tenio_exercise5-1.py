#Ex 5_1 projectile trjectory , return list f xy values , call function and plot 

import matplotlib.pyplot as plt
import math

def projectile (x , y, dx , dy, t , g , v ,angl_1sin,angl_1cos,dt ):   #def statement 
    
    # lists to store variables
    t = [0]
    dx = [v * angl_1cos]
    dy = [v * angl_1sin]
    x = [0]
    y = [0]
   

    #define and update position variables to produce values 
    tm_stp = 0
    while y[tm_stp] >= 0 :
       t.append(t[tm_stp] + dt)
       dx.append(dx[tm_stp])
       x.append(x[tm_stp] + dt * dx[tm_stp])
       dy.append(dy[tm_stp] - g * dt)
       y.append(y[tm_stp] + dt * dy[tm_stp])
       tm_stp += 1
    
    return x , y 


x = []
y = []
dx= []
dy= []
t= []
g = 9.8  # acceleration due to gravity
v = 80  # arbitrary initial velocity value
angl_1sin = 0.86602540378  #sin 60 deg
angl_1cos = 0.5            #cos 60 deg 
dt = .5  # arbitrary time step
angl_2sin = 0.70710678118  #sin 45 deg
angl_2cos = 0.70710678118  #cos 45 deg 

x , y  = projectile (x , y, dx , dy, t , g , v ,angl_1sin,angl_1cos,dt )  #returned list of x , y values 
x_o , y_o  = projectile (x , y, dx , dy, t , g , v ,angl_2sin,angl_2cos,dt )

print (x , y )


plt.plot (x , y , '--')
plt.plot (x_o , y_o, '--')

plt.xlabel('Distance (m)')
plt.ylabel('Height (m)')
plt.title('Projectile Motion Of 2 Objects')
plt.show ()






        






