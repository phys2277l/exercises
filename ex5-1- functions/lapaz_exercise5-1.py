import numpy as np
import math as mt
import matplotlib.pyplot as plt


listx = []
listy = []



Vt = 36
g = 9.8
y0 = 1
x0 = 1
t = 0
angle1 = mt.pi*(7/9)
angle2 = mt.pi*(4/5)


def sigma (x0, y0, Vt, angle, g = 9.8):
    listx = []
    listy = []
    vx0 = Vt * mt.cos(angle)
    vy0 = Vt * mt.sin(angle)
    t = 0
    while y0 > 0:
        t += 0.2
        x_1 = x0 + vx0*(t)
        y_1 = y0 + vy0*(t) - 0.5*(g*t**2)
        if y_1 <= 0:
            break
        listy.append(y_1)
        listx.append(x_1)
    return listx, listy


listx, listy = sigma(x0, y0, Vt, angle1, g = 9.8)
listx1, listy1 = sigma(x0, y0, Vt, angle2, g = 9.8)

print(f"listx: {listx}")
print(f"listy: {listy}")

plt.plot(listx, listy)
plt.plot(listx1, listy1)
plt.show()