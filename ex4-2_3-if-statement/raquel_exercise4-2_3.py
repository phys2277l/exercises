#Number_2 

def fibonacci_even_list(limit):
    fib_list = [1, 2]
    while fib_list[-1] + fib_list[-2] <= limit:
        fib_list.append(fib_list[-1] + fib_list[-2])
    return [x for x in fib_list if x % 2 == 0]

print(fibonacci_even_list(1000))

def fibonacci_even_sum(limit):
    fib_list = [1, 2]
    while fib_list[-1] + fib_list[-2] <= limit:
        fib_list.append(fib_list[-1] + fib_list[-2])
    return sum(x for x in fib_list if x % 2 == 0)

print(fibonacci_even_sum(1000))

#Number_3

def sum_of_multiples(limit):
    return sum(x for x in range(limit) if x % 3 == 0 or x % 5 == 0)
print('\n')
print(sum_of_multiples(1000))


