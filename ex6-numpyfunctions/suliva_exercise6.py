import numpy as np


#Number 1
arr1 = np.array ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print ("The spliced elements are", arr1[:3], '\n')


#Number 2
arr2 = np.array ([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print ("The accessed value in the second row and third column is", arr2 [1,2], '\n')


#Number 3

#randint version
arr3_1 = np.random.randint (10, size=(4,4))
arr3_1 [1] = arr3_1 [3]
print ("With replaced values", arr3_1, '\n')

#rand version
arr3_2 = np.random.rand (4, 4)
arr3_2 [1] = arr3_2 [3]
print ("With replaced values", arr3_2, '\n')


#Number 4
arr4 = np.random.randint (10, size = 5)
print ("Mean is", np.mean (arr4))
print ("Median is", np.median (arr4))
print ("Standard Deviation is", np.std (arr4), '\n')


#Number 5
arr5 = np.random.randint (10, size = (3, 3))
print ("The determinant is", np.linalg.det(arr5), '\n')


#Number 6
arr6 = np.random.randint (10, size = (3, 3))
print ("The inverse is", np.linalg.inv(arr6), '\n')

#Number 7
arr7 = np.random.randint (10, size = (3,3))
print ("The Frobenius norm is", np.linalg.norm(arr7), '\n')


#Number 8
arr8 = np.random.randint (10, size = (3, 3))
arr9 = np.random.randint (10, size = (3, 1))
print ("The dot product is", np.dot(arr8, arr9), '\n')

