import numpy as np
#1

sigma1 = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
sigma1[0:3]

2#

sigma2 = np.array([[4, 5, 6], [7, 8, 9]])
sigma2[1, 2]

#3

sigma3 = np.random.randint(60, size = (4, 4))

sigma3[1] = sigma3[3]

print('2D array shape [4,4]:', sigma3)


#4

sigma4 = np.random.randint(50, size = 5)
print('1D array length 5:', sigma4)
print ('Mean:', np.mean(sigma4))
print ('Median:', np.median(sigma4))
print ('Standard Deviation:', np.std(sigma4))


#5

sigma5 = np.random.randint(40, size = (3, 3))
print('2D array shape [3,3]:', sigma5)
print('Determinant:', np.linalg.det(sigma5))

#6

sigma6 = np.random.randint(20, size = (3, 3))
print('2D array shape [3,3]:', sigma6)
print('Inverse:',  np.linalg.inv(sigma6))

#7

sigma7 = np.random.randint(35, size = (3, 3))
print('2D array shape [3,3]:', sigma7)
print('Frobenius norm:',  np.linalg.norm(sigma7))

#8

sigma8_1 = np.random.randint(35, size = (3, 3))
sigma8_2 = np.random.randint(35, size = (3, 1))


print ('Sigma1:', sigma8_1)
print ('Sigma2:', sigma8_2)
print ('Dot product:',  np.dot(sigma8_1, sigma8_2))