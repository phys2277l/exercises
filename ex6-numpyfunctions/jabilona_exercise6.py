#RAN THROUGH JUPYTER NOTEBOOK
import numpy as np
#1
arr1=np.array([1,2,3,4,5])

arr1[0:3]

#2
arr2=np.array([[1,2,3],[4,5,6]])
arr2[1,2]

#3
arr3_1=np.random.randint(10, size=(4,4))
arr3_1[1]=arr3_1[3]

print(arr3_1)

arr3_2=np.random.rand(4,4)
arr3_2[1]=arr3_2[3]

print(arr3_2)

#4
arr4=np.random.rand(5)

mean=np.mean(arr4)
std=np.std(arr4)
median=np.median(arr4)

print(mean)
print(std)
print(median)

#5
arr5=np.random.rand(2,2)
det=np.linalg.det(arr5)

print(det)

#6
arr6=np.random.rand(3,3)
inverse=np.linalg.inv(arr6)

print(inverse)

#7
arr7=np.random.rand(3,3)
norm=np.linalg.norm(arr7)

print(norm)

#8
arr8=np.random.rand(3,3)
print(arr8)
arr9=np.random.rand(3,1)
print(arr9)

dot=np.dot(arr8,arr9)

print(dot)

