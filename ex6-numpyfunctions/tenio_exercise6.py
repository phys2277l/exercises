# Create a 1D array with values from 1 to 10. Use slicing to create a new array that contains the first three elements

import numpy as np 

line = "__________ʕ•́ᴥ•̀ʔっ__________"
x = line.center(50)
print (x)
print ()
arr1 = np.array ([1,2,3,4,5,6,7,8,9,10])
print ('array 1 : ' , arr1)
print('first three elements',arr1[0:3])
print (x)
print ()
#Create a 2D array with shape (3, 3) and values from 1 to 9. Use indexing to access the value in the second row and third column.

arr2 = np.array ([[4,1,1],[2,6,4]])
print ('array 2:',arr2)
print('value of second row third column',arr2[1,2])
print (x)
print ()
#Create a 2D array of shape (4, 4) with random values using numpy.random.rand(). Use indexing to replace the values in the second row with the values in the fourth row.

arr3 = np.random.rand(4,4)
print ('random 2D array' ,arr3)
arr3[1, :] = arr3 [3,:]
print ()
print ('replaced random 2D array' ,arr3)
print (x)
print ()
#Create a 1D array of length 5 with random values. Calculate the mean, median, and standard deviation of the values using numpy functions.

arr4 = np.random.rand(1,5)
print ('random 1D array',arr4)
print ()
mn = np.mean(arr4)
med = np.median(arr4)
st = np.std(arr4)

print ('mean:',mn)
print ('median:',med)
print('standard deviation:',st)
print (x)
print ()
#Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.det() function to find the determinant of the array.

arr5 = np.random.rand (3,3)
det = np.linalg.det(arr5)

print('random 2D array:', arr5)
print ()
print('determinant:',det)
print (x)
print ()
#Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.inv() function to find the inverse of the array.
arr6 = np.random.rand (3,3)
inv = np.linalg.inv(arr6)

print('random 2D array:', arr6)
print ()
print('inverse:',inv)
print (x)
print ()
#Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.norm() function to find the Frobenius norm of the array.
arr7 = np.random.rand (3,3)
norm = np.linalg.norm(arr7)

print('random 2D array:', arr7)
print ()
print('frobenius norm:',norm)
print (x)
print ()
#Create two 2D arrays with shapes (3, 3) and (3, 1) with random values. Use the numpy.dot() function to find the dot product of the arrays.
arr8 = np.random.rand (3,3)
arr9 = np.random.rand (3,1)

dot = np.dot(arr8 , arr9 )

print('first random 2D array:', arr8)
print('second random 2D array:', arr9)
print ()
print('dot product:',dot )
print (x)