#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


#1 Create a 1D array with values from 1 to 10. Use slicing to create a new array that contains the first three elements.

A=np.array([1,2,3,4,5,6,7,8,9,10])
A[0:3]


# In[3]:


#2 Create a 2D array with shape (3, 3) and values from 1 to 9. Use indexing to access the value in the second row and third column.

B=np.array([[1,2,3],
            [4,5,6],
            [7,8,9]])
B[1,2]


# In[4]:


#3 Create a 2D array of shape (4, 4) with random values using numpy.random.rand(). Use indexing to replace the values in the second row with the values in the fourth row.

C=np.random.rand(4,4)
print(C)
C1=np.array([C[0],C[3],C[2],C[3]])
print(C1)


# In[5]:


#4 Create a 1D array of length 5 with random values. Calculate the mean, median, and standard deviation of the values using numpy functions.
D=np.random.rand(1,5)
print(D)
np.mean(D)
np.median(D)
np.std(D)


# In[9]:


#5 Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.det() function to find the determinant of the array.

E=np.random.rand(3,3)
print(E)
np.linalg.det(E)


# In[10]:


#6 Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.inv() function to find the inverse of the array.

F=np.random.rand(3,3)
print(F)
np.linalg.inv(F)


# In[11]:


#7 Create a 2D array of shape (3, 3) with random values. Use the numpy.linalg.norm() function to find the Frobenius norm of the array.

G=np.random.rand(3,3)
print(G)
np.linalg.norm(G)


# In[12]:


#8 Create two 2D arrays with shapes (3, 3) and (3, 1) with random values. Use the numpy.dot() function to find the dot product of the arrays.

H1=np.random.rand(3,3)
H2=np.random.rand(3,1)
print(H1)
print(H2)
np.dot(H1,H2)


# In[ ]:




