#Number 1 

import numpy as np

arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

new_arr = arr[:3]
print("\t")
print('Number 1:')
print(new_arr)

#Number 2 

arr1 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

value = arr1[1, 2]
print("\t")
print('Number 2:')
print(value)

#Number 3

arr2 = np.random.rand(4,4)

arr2[1, :] = arr2[3, :]
print("\t")
print('Number 3:')
print(arr2)

#Number 4

arr3 = np.random.rand(5)

mean = np.mean(arr3)
median = np.median(arr3)
std = np.std(arr3)

print("\t")
print('Number 4:')
print(arr3)
print(mean)
print(median)
print(std)

#Number 5

arr4 = np.random.rand(4,4)

det = np.linalg.det(arr4)
print("\t")
print('Number 5:')
print(arr4)
print(det)

#Number 6 

arr5 = np.random.rand(3,3)

inv = np.linalg.inv(arr5)

print("\t")
print('Number 6:')
print(arr5)
print(inv)

#Number 7

arr6 = np.random.rand(3,3)

frob = np.linalg.norm(arr6)

print("\t")
print('Number 7:')
print(arr6)
print(frob)

#Number 8

arr7 = np.random.rand(3,3)
arr8 = np.random.rand(3,1)

result = np.dot(arr7, arr8)

print("\t")
print('Number 8:')
print(result)
