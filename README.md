# Exercises

## Add your files

Filename: (lowercase)

    lastname_exercise#.py


Commit message: (lowercase)

    lastname_exercise#

## List of exercises
- ex1 - Quadratic Formula
- ex2 - Kinematic Equation
- ex2 - Lists
- ex4 - If-statements
- ex5 - Functions
- ex6 - Numpy
