def sigma_matrix(matrix):
    for i in range(3):
        matrix[i].append(matrix[i][0])
        matrix[i].append(matrix[i][1])
        matrix[i].append(matrix[i][2])

    return matrix

def determinant(matrix):
    a = matrix[0][0] * matrix[1][1] * matrix[2][2]
    b = matrix[0][1] * matrix[1][2] * matrix[2][0]
    c = matrix[0][2] * matrix[1][0] * matrix[2][1]
    d = matrix[2][0] * matrix[1][1] * matrix[0][2]
    e = matrix[2][1] * matrix[1][2] * matrix[0][0]
    f = matrix[2][2] * matrix[1][0] * matrix[0][1]

    return a + b + c - d - e - f


matrix = [[69, 41, 32],
          [-69, 96, 23],
          [55, 79, -40]]


matrix = sigma_matrix(matrix)


det = determinant(matrix)

print(matrix)
print("Determinant of the matrix:", det)
