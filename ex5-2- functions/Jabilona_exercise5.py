#2.1 MATRIX CONVERSION 

def convert(mat):
    for i in range (3):
        mat[i].append(mat[0][i])
        mat[i].append(mat[1][i])
        mat[i].append(mat[2][i])
    print(mat)
    return mat


mat=[[1,2,3],[4,5,6],[7,8,9]]
mat1=convert(mat)

#2.2 DETERMINANT

def det(mat):
    aei = mat[0][0]*mat[1][1]*mat[2][2]
    bfg = mat[0][1]*mat[1][2]*mat[2][3]
    cdh = mat[0][2]*mat[1][3]*mat[2][4]
    afh = mat[0][3]*mat[1][2]*mat[2][1]
    bdi = mat[0][4]*mat[1][3]*mat[2][2]
    ceg = mat[0][5]*mat[1][4]*mat[2][3]

    determinant= aei + bfg + cdh - afh - bdi - ceg

    return determinant

mat=[[1,2,3,2,1,0],[4,5,6,5,4,1],[7,8,9,7,6,5]]
determinant=det(mat)
print(determinant)
